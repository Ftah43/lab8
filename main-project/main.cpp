#include <iostream>
#include <iomanip>
#include <cstring>
#include <cstdlib>

using namespace std;

#include "phone_conversations.h"
#include "file_reader.h"
#include "constants.h"

// ��������� ��� �������������� � �������� ���� ��������� � �������� ����� i, ��� ��������
// �������� � arr[]. n - ������ ����

void heapify(int arr[], int n, int i)
{
    int largest = i;
    // �������������� ���������� ������� ��� ������
    int l = 2 * i + 1; // ����� = 2*i + 1
    int r = 2 * i + 2; // ������ = 2*i + 2

    // ���� ����� �������� ������� ������ �����
    if (l < n && arr[l] < arr[largest])
        largest = l;

    // ���� ������ �������� ������� ������, ��� ����� ������� ������� �� ������ ������
    if (r < n && arr[r] < arr[largest])
        largest = r;

    // ���� ����� ������� ������� �� ������
    if (largest != i)
    {
        swap(arr[i], arr[largest]);

        // ���������� ����������� � �������� ���� ���������� ���������
        heapify(arr, n, largest);
    }
}

// �������� �������, ����������� ������������� ����������
void heapSort(int arr[], int n)
{
    // ���������� ���� (�������������� ������)
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify(arr, n, i);

    // ���� �� ������ ��������� �������� �� ����
    for (int i = n - 1; i >= 0; i--)
    {
        // ���������� ������� ������ � �����
        swap(arr[0], arr[i]);

        // �������� ��������� heapify �� ����������� ����
        heapify(arr, i, 0);
    }
}

/* ��������������� ������� ��� ������ �� ����� ������� ������� n*/
void printArray(struct phone_conversations* arr[], int n)
{
    for (int i = 0; i < n; ++i)
        cout << arr[i] << " ";
    cout << "\n";
}

void quicksort(int* mas, int first, int last)
{
    int mid, count;
    int f = first, l = last;
    mid = mas[(f + l) / 2]; //���������� �������� ��������
    do
    {
        while (mas[f] < mid) f++;
        while (mas[l] > mid) l--;
        if (f <= l) //������������ ���������
        {
            count = mas[f];
            mas[f] = mas[l];
            mas[l] = count;
            f++;
            l--;
        }
    } while (f < l);
    if (first < l) quicksort(mas, first, l);
    if (f < last) quicksort(mas, f, last);
}

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ #8. GIT\n";
    cout << "������� #9. ���������� ���������\n";
    cout << "�����: ����� �������\n";
    cout << "������: 11\n";
    phone_conversations* subscriptions[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", subscriptions, size);
        cout << "***** ���������� ��������� *****\n\n";
        for (int i = 0; i < size; i++)
        {
            /********** ����� ������ **********/
            cout << "�����........: ";
            // ����� ������
            cout << subscriptions[i]->number << " ";
            cout << '\n';
            /********** ����� ���� **********/
            // ����� ����
            cout << "���� ������.....: ";
            cout << setw(2) << setfill('0') << subscriptions[i]->date1.day << '-';
            // ����� ������
            cout << setw(2) << setfill('0') << subscriptions[i]->date1.month << '-';
            // ����� �����
            cout << setw(2) << setfill('0') << subscriptions[i]->date1.year;
            cout << '\n';
            /********** ����� ������� ������ ��������� **********/
            // ����� ����
            cout << "����� ������ ���������...: ";
            cout << setw(2) << setfill('0') << subscriptions[i]->start.hour << ':';
            // ����� �����
            cout << setw(2) << setfill('0') << subscriptions[i]->start.minute << ':';
            // ����� ������
            cout << setw(2) << setfill('0') << subscriptions[i]->start.second;
            cout << '\n';
            /********** ����� ����������������� ��������� **********/
            // ����� ����
            cout << "����������������� ���������...: ";
            cout << setw(2) << setfill('0') << subscriptions[i]->longs.hour << ':';
            // ����� �����
            cout << setw(2) << setfill('0') << subscriptions[i]->longs.minute << ':';
            // ����� ������
            cout << setw(2) << setfill('0') << subscriptions[i]->longs.second;
            cout << '\n';
            /********** ����� ������ **********/
            cout << "�����........: ";
            // ����� ������
            cout << subscriptions[i]->tarif << " ";
            cout << '\n';
            /********** ����� ��������� **********/
            cout << "��������� 1 ������ ���������........: ";
            // ����� ���������
            cout << subscriptions[i]->cost << " ";
            cout << '\n';
            cout << '\n';
        }
        cout << "����� ��������������� �� ������ ����������: " << endl;
        cout << '\n';
        for (int i = 0; i < size; i++)
        {
            if (strcmp(subscriptions[i]->tarif, "���������") == 0) {
                /********** ����� ������ **********/
                cout << "�����........: ";
                // ����� ������
                cout << subscriptions[i]->number << " ";
                cout << '\n';
                /********** ����� ���� **********/
                // ����� ����
                cout << "���� ������.....: ";
                cout << setw(2) << setfill('0') << subscriptions[i]->date1.day << '-';
                // ����� ������
                cout << setw(2) << setfill('0') << subscriptions[i]->date1.month << '-';
                // ����� �����
                cout << setw(2) << setfill('0') << subscriptions[i]->date1.year;
                cout << '\n';
                /********** ����� ������� ������ ��������� **********/
                // ����� ����
                cout << "����� ������ ���������...: ";
                cout << setw(2) << setfill('0') << subscriptions[i]->start.hour << ':';
                // ����� �����
                cout << setw(2) << setfill('0') << subscriptions[i]->start.minute << ':';
                // ����� ������
                cout << setw(2) << setfill('0') << subscriptions[i]->start.second;
                cout << '\n';
                /********** ����� ����������������� ��������� **********/
                // ����� ����
                cout << "����������������� ���������...: ";
                cout << setw(2) << setfill('0') << subscriptions[i]->longs.hour << ':';
                // ����� �����
                cout << setw(2) << setfill('0') << subscriptions[i]->longs.minute << ':';
                // ����� ������
                cout << setw(2) << setfill('0') << subscriptions[i]->longs.second;
                cout << '\n';
                /********** ����� ������ **********/
                cout << "�����........: ";
                // ����� ������
                cout << subscriptions[i]->tarif << " ";
                cout << '\n';
                /********** ����� ��������� **********/
                cout << "��������� 1 ������ ���������........: ";
                // ����� ���������
                cout << subscriptions[i]->cost << " ";
                cout << '\n';
                cout << '\n';
            }
        }
        cout << "����� ��������������� �� ������ 21 ���� ����������: " << endl;
        cout << '\n';
        for (int i = 0; i < size; i++)
        {
            if (subscriptions[i]->date1.month == 11 && subscriptions[i]->date1.year == 21) {
                /********** ����� ������ **********/
                cout << "�����........: ";
                // ����� ������
                cout << subscriptions[i]->number << " ";
                cout << '\n';
                /********** ����� ���� **********/
                // ����� ����
                cout << "���� ������.....: ";
                cout << setw(2) << setfill('0') << subscriptions[i]->date1.day << '-';
                // ����� ������
                cout << setw(2) << setfill('0') << subscriptions[i]->date1.month << '-';
                // ����� �����
                cout << setw(2) << setfill('0') << subscriptions[i]->date1.year;
                cout << '\n';
                /********** ����� ������� ������ ��������� **********/
                // ����� ����
                cout << "����� ������ ���������...: ";
                cout << setw(2) << setfill('0') << subscriptions[i]->start.hour << ':';
                // ����� �����
                cout << setw(2) << setfill('0') << subscriptions[i]->start.minute << ':';
                // ����� ������
                cout << setw(2) << setfill('0') << subscriptions[i]->start.second;
                cout << '\n';
                /********** ����� ����������������� ��������� **********/
                // ����� ����
                cout << "����������������� ���������...: ";
                cout << setw(2) << setfill('0') << subscriptions[i]->longs.hour << ':';
                // ����� �����
                cout << setw(2) << setfill('0') << subscriptions[i]->longs.minute << ':';
                // ����� ������
                cout << setw(2) << setfill('0') << subscriptions[i]->longs.second;
                cout << '\n';
                /********** ����� ������ **********/
                cout << "�����........: ";
                // ����� ������
                cout << subscriptions[i]->tarif << " ";
                cout << '\n';
                /********** ����� ��������� **********/
                cout << "��������� 1 ������ ���������........: ";
                // ����� ���������
                cout << subscriptions[i]->cost << " ";
                cout << '\n';
                cout << '\n';
            }
        }
        int* a = new int[size];
        for (int i = 0; i < size; i++)
        {
            int k = subscriptions[i]->longs.hour * 3600 + subscriptions[i]->longs.minute * 60 + subscriptions[i]->longs.second;
            cout << k << endl;
            a[i] = k;
        }
        heapSort(a, size);
        cout << '\n';
        for (int i = 0; i < size; i++)
        {
            cout << a[i] << endl;
            cout << '\n';
            for (int k = 0;k < size;k++) {
                if (subscriptions[k]->longs.hour * 3600 + subscriptions[k]->longs.minute * 60 + subscriptions[k]->longs.second == a[i]) {
                    /********** ����� ������ **********/
                    cout << "�����........: ";
                    // ����� ������
                    cout << subscriptions[k]->number << " ";
                    cout << '\n';
                    /********** ����� ���� **********/
                    // ����� ����
                    cout << "���� ������.....: ";
                    cout << setw(2) << setfill('0') << subscriptions[k]->date1.day << '-';
                    // ����� ������
                    cout << setw(2) << setfill('0') << subscriptions[k]->date1.month << '-';
                    // ����� �����
                    cout << setw(2) << setfill('0') << subscriptions[k]->date1.year;
                    cout << '\n';
                    /********** ����� ������� ������ ��������� **********/
                    // ����� ����
                    cout << "����� ������ ���������...: ";
                    cout << setw(2) << setfill('0') << subscriptions[k]->start.hour << ':';
                    // ����� �����
                    cout << setw(2) << setfill('0') << subscriptions[k]->start.minute << ':';
                    // ����� ������
                    cout << setw(2) << setfill('0') << subscriptions[k]->start.second;
                    cout << '\n';
                    /********** ����� ����������������� ��������� **********/
                    // ����� ����
                    cout << "����������������� ���������...: ";
                    cout << setw(2) << setfill('0') << subscriptions[k]->longs.hour << ':';
                    // ����� �����
                    cout << setw(2) << setfill('0') << subscriptions[k]->longs.minute << ':';
                    // ����� ������
                    cout << setw(2) << setfill('0') << subscriptions[k]->longs.second;
                    cout << '\n';
                    /********** ����� ������ **********/
                    cout << "�����........: ";
                    // ����� ������
                    cout << subscriptions[k]->tarif << " ";
                    cout << '\n';
                    /********** ����� ��������� **********/
                    cout << "��������� 1 ������ ���������........: ";
                    // ����� ���������
                    cout << subscriptions[k]->cost << " ";
                    cout << '\n';
                    cout << '\n';
                }
            }
        }
        cout << "��������� �������������� �� ������: " << endl;
        cout << '\n';
        int* c = new int[size];
        int* c1 = new int[size];
        for (int i = 0; i < size; i++)
        {
            int v = 1000000;
            int y = 0;
            if ((subscriptions[i]->number)[2] == '3') {
                for (int k = 4;k < strlen(subscriptions[k]->number);k++) {
                    char m = (subscriptions[i]->number)[k];
                    int n = ((int)m - '0') * v;
                    y += n;
                    c[i] = y;
                    c1[i] = y;
                    v /= 10;
                }
            }
        }
        cout << '\n';
        int* b = new int[size];
        int* b1 = new int[size];
        for (int i = 0; i < size; i++)
        {
            int v = 1000000;
            int y = 0;
            if ((subscriptions[i]->number)[2] == '2'){
                for (int k = 4;k < strlen(subscriptions[k]->number);k++) {
                    char m = (subscriptions[i]->number)[k];
                    int n = ((int)m - '0') * v;
                    y += n;
                    b[i] = y;
                    b1[i] = y;
                    v /= 10;
                }
            }
        }
        quicksort(c, 0, size - 1);
        quicksort(b, 0, size - 1);
        for (int i = 0;i < size;i++) {
            if(b[i]>0)
            cout << b[i] << endl;
        }
        cout << '\n';
        for (int i = 0;i < size;i++) {
            if (b[i] > 0) {
                for (int k = 0;k < size;k++) {
                    if (b[i] == b1[k]) {
                        /********** ����� ������ **********/
                        cout << "�����........: ";
                        // ����� ������
                        cout << subscriptions[k]->number << " ";
                        cout << '\n';
                        /********** ����� ���� **********/
                        // ����� ����
                        cout << "���� ������.....: ";
                        cout << setw(2) << setfill('0') << subscriptions[k]->date1.day << '-';
                        // ����� ������
                        cout << setw(2) << setfill('0') << subscriptions[k]->date1.month << '-';
                        // ����� �����
                        cout << setw(2) << setfill('0') << subscriptions[k]->date1.year;
                        cout << '\n';
                        /********** ����� ������� ������ ��������� **********/
                        // ����� ����
                        cout << "����� ������ ���������...: ";
                        cout << setw(2) << setfill('0') << subscriptions[k]->start.hour << ':';
                        // ����� �����
                        cout << setw(2) << setfill('0') << subscriptions[k]->start.minute << ':';
                        // ����� ������
                        cout << setw(2) << setfill('0') << subscriptions[k]->start.second;
                        cout << '\n';
                        /********** ����� ����������������� ��������� **********/
                        // ����� ����
                        cout << "����������������� ���������...: ";
                        cout << setw(2) << setfill('0') << subscriptions[k]->longs.hour << ':';
                        // ����� �����
                        cout << setw(2) << setfill('0') << subscriptions[k]->longs.minute << ':';
                        // ����� ������
                        cout << setw(2) << setfill('0') << subscriptions[k]->longs.second;
                        cout << '\n';
                        /********** ����� ������ **********/
                        cout << "�����........: ";
                        // ����� ������
                        cout << subscriptions[k]->tarif << " ";
                        cout << '\n';
                        /********** ����� ��������� **********/
                        cout << "��������� 1 ������ ���������........: ";
                        // ����� ���������
                        cout << subscriptions[k]->cost << " ";
                        cout << '\n';
                        cout << '\n';
                    }
                }
            }
        }
        cout << '\n';
        for (int i = 0;i < size;i++) {
            if (c[i] > 0)
                cout <<c[i] << endl;
        }
        cout << '\n';
        for (int i = 0;i < size;i++) {
            if (c[i] > 0) {
                for (int k = 0;k < size;k++) {
                    if (c[i] == c1[k]) {
                        /********** ����� ������ **********/
                        cout << "�����........: ";
                        // ����� ������
                        cout << subscriptions[k]->number << " ";
                        cout << '\n';
                        /********** ����� ���� **********/
                        // ����� ����
                        cout << "���� ������.....: ";
                        cout << setw(2) << setfill('0') << subscriptions[k]->date1.day << '-';
                        // ����� ������
                        cout << setw(2) << setfill('0') << subscriptions[k]->date1.month << '-';
                        // ����� �����
                        cout << setw(2) << setfill('0') << subscriptions[k]->date1.year;
                        cout << '\n';
                        /********** ����� ������� ������ ��������� **********/
                        // ����� ����
                        cout << "����� ������ ���������...: ";
                        cout << setw(2) << setfill('0') << subscriptions[k]->start.hour << ':';
                        // ����� �����
                        cout << setw(2) << setfill('0') << subscriptions[k]->start.minute << ':';
                        // ����� ������
                        cout << setw(2) << setfill('0') << subscriptions[k]->start.second;
                        cout << '\n';
                        /********** ����� ����������������� ��������� **********/
                        // ����� ����
                        cout << "����������������� ���������...: ";
                        cout << setw(2) << setfill('0') << subscriptions[k]->longs.hour << ':';
                        // ����� �����
                        cout << setw(2) << setfill('0') << subscriptions[k]->longs.minute << ':';
                        // ����� ������
                        cout << setw(2) << setfill('0') << subscriptions[k]->longs.second;
                        cout << '\n';
                        /********** ����� ������ **********/
                        cout << "�����........: ";
                        // ����� ������
                        cout << subscriptions[k]->tarif << " ";
                        cout << '\n';
                        /********** ����� ��������� **********/
                        cout << "��������� 1 ������ ���������........: ";
                        // ����� ���������
                        cout << subscriptions[k]->cost << " ";
                        cout << '\n';
                        cout << '\n';
                    }
                }
            }
        }
        for (int i = 0; i < size; i++)
        {
            delete subscriptions[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}