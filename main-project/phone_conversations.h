#ifndef PHONE_CONVERSATIONS_H
#define PHONE_CONVERSATIONS_H

#include "constants.h"

struct date
{
    int day;
    int month;
    int year;
};
 
struct time1
{
    int hour;
    int minute;
    int second;
};

struct phone_conversations
{
    char number[MAX_STRING_SIZE];
    date date1;
    time1 start;
    time1 longs;
    char tarif[MAX_STRING_SIZE];
    double cost;
};

#endif